import pytest


# Leap Year class implementation
class TestForLeapYear:

    # Production Code
    # Function to check if the year is Leap year or not
    def isLeapYear(self, year):
        if self.isMultiple(year, 400):
            # returns True for 400, 800, 1200...
            return True
        else:
            if self.isMultiple(year, 4):
                if self.isMultiple(year, 100):
                    # returns False for 100, 200, 500, 700...
                    return False
                # returns True for 2024, 404, 808, 4, 12...
                return True
        # returns False if none of the above conditions matches (default)
        return False

    # Utility Functions

    # Function to check if mod divides the value completely (i.e. 0 remainder)
    def isMultiple(self, value, mod):
        return (value % mod) == 0

    # Function to match the expected value and return value
    def check(self, value, expectedvalue):
        retVal = self.isLeapYear(value)
        assert retVal == expectedvalue


# Get "False" when passed 1
def test_getFalseWithPassed1():
    obj = TestForLeapYear()
    obj.check(1, False)


# Get "False" when passed 2
def test_getFalseWithPassed2():
    obj = TestForLeapYear()
    obj.check(2, False)


# Get "False" when passed 3
def test_getFalseWithPassed3():
    obj = TestForLeapYear()
    obj.check(3, False)


# Get "True" when passed 400
def test_getTrueWithPassed400():
    obj = TestForLeapYear()
    obj.check(400, True)


# Get "True" when passed 800 (multiple of 400)
def test_getTrueWithPassed800():
    obj = TestForLeapYear()
    obj.check(800, True)


# Get "True" when passed 804 (multiple of 4)
def test_getTrueWithPassed804():
    obj = TestForLeapYear()
    obj.check(804, True)


# Get "False" when passed 500 (multiple of 100 but not 400)
def test_getFalseWithPassed500():
    obj = TestForLeapYear()
    obj.check(500, False)


# Get "False" when passed 2022 (not a multiple of 4)
def test_getFalseWithPassed2022():
    obj = TestForLeapYear()
    obj.check(2022, False)


# Get "True" when passed 2024 (multiple of 4 but not 100)
def test_getTrueWithPassed2024():
    obj = TestForLeapYear()
    obj.check(2024, True)